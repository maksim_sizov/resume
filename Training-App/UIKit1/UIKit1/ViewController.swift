//
//  ViewController.swift
//  UIKit1
//
//  Created by MaxOS on 24.05.2023.
//

import UIKit

class ViewController: UIViewController {
    
    var uiElements = [  "Slider",
                        "Lable",
                        "TextField",
                        "DatePicker",
                        "SegmentControl",
                        "SwitchLable",
                        "DoneButton",
                        "Cancel"]
    
    var selectedElement: String?
    
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var lable: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var switchLable: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        slider.value = 1
        lable.text = String(slider.value)
        lable.font = lable.font.withSize(35)
        lable.textAlignment = .center
        lable.numberOfLines = 2
        
        segmentControl.insertSegment(withTitle: "Third", at: 2, animated: true)
        
        slider.minimumValue = 0
        slider.maximumValue = 1
        slider.minimumTrackTintColor = .yellow
        slider.maximumTrackTintColor = .red
        slider.thumbTintColor = .blue
        
        
        datePicker.datePickerMode = .date
        datePicker.locale = Locale(identifier: "ru_RU")
        
        choiseUiElement()
        createToolBar()
    }
    func hideAllElements () {
        slider.isHidden = true
        lable.isHidden = true
        datePicker.isHidden = true
        segmentControl.isHidden = true
        switchLable.isHidden = true
        doneButton.isHidden = true
    }
    func showAllElements () {
        slider.isHidden = false
        lable.isHidden = false
        datePicker.isHidden = false
        segmentControl.isHidden = false
        switchLable.isHidden = false
        doneButton.isHidden = false
    }
    func choiseUiElement () {
        let elementPicker = UIPickerView()
        elementPicker.delegate = self
        
        textField.inputView = elementPicker
        
        elementPicker.backgroundColor = .gray
    }
    func createToolBar () {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissKeyboard))
        toolBar.setItems([doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        textField.inputAccessoryView = toolBar
        toolBar.barTintColor = .darkGray
        toolBar.tintColor = .white
    }
    @objc func dismissKeyboard () {
        view.endEditing(true)
    }
    
    @IBAction func choiceSegmented(_ sender: UISegmentedControl) {
        lable.isHidden = false
        switch segmentControl.selectedSegmentIndex {
        case 0 :
            lable.text = "First segment"
            lable.textColor = .red
        case 1 :
            lable.text = "Second segment"
            lable.textColor = .yellow
        case 2 :
            lable.text = "Third segmented"
            lable.textColor = .green
        default:
            print("Error!!!!0!!!!!0!(!!(!!)!)!)!!!_!")
        }
    }
    @IBAction func sliderAction(_ sender: UISlider) {
        lable.text = String(sender.value)
        
        let backgroundColor = self.view.backgroundColor
        
        self.view.backgroundColor = backgroundColor?.withAlphaComponent(CGFloat(sender.value))
        
    }
    func showAlert () {
        let alert = UIAlertController(title: "Wrong format", message: "Please enter your name", preferredStyle: .alert)
        present(alert, animated: true) {
            self.lable.text = "Try now!"
        }
        alert.addTextField { nameTF in
            nameTF.placeholder = "Please, enter your name"
        }
        let okAction = UIAlertAction(title: "Ok", style: .default)
        alert.addAction(okAction)
        
    }
    
    @IBAction func donePressed(_ sender: UIButton) {
        
        guard textField.text?.isEmpty == false else { return }
        let stringText = textField.text!
        for char in stringText {
            if char == " " || char == "1" || char == "2" || char == "3" || char == "4" || char == "5" || char == "6" || char == "7" || char == "8" || char == "9" || char == "0" {
                showAlert()
            } else {
                continue
            }
        }
        
        if let _ = Double(textField.text!) {
            
            showAlert()
        } else {
            lable.text = textField.text
            textField.text = nil
        }
    }
    @IBAction func cangeDate(_ sender: UIDatePicker) {
        let dateFormater = DateFormatter()
        dateFormater.dateStyle = .full
        let dateValue = dateFormater.string(from: sender.date)
        lable.text = dateValue
    }
    @IBAction func switchAction(_ sender: UISwitch) {
        
        segmentControl.isHidden.toggle()
        lable.isHidden.toggle()
        slider.isHidden.toggle()
        textField.isHidden.toggle()
        datePicker.isHidden.toggle()
        doneButton.isHidden.toggle()
        
        if sender.isOn {
            switchLable.text = "Скрыть все элементы"
        } else {
            switchLable.text = "Показать все элементы"
        }
    }
    
    
}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return uiElements.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return uiElements[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedElement = uiElements[row]
        textField.text = selectedElement
        
        switch row {
        case 0:
            hideAllElements()
            segmentControl.isHidden = false
        case 2:
            hideAllElements()
            lable.isHidden = false
        case 3:
            hideAllElements()
            slider.isHidden = false
        case 4:
            hideAllElements()
        case 5:
            hideAllElements()
            doneButton.isHidden = false
        case 6:
            hideAllElements()
            datePicker.isHidden = false
        case 7:
            showAllElements()
        default:
            hideAllElements()
        }
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    viewForRow row: Int,
                    widthForComponent component: Int,
                    reusing view: UIView?) -> UIView {
        
        var pickerViewLable = UILabel()
        
        if let currentLable = view as? UILabel {
            pickerViewLable = currentLable
        } else {
            pickerViewLable = UILabel()
        }
        
        pickerViewLable.textColor = .white
        pickerViewLable.textAlignment = .center
        pickerViewLable.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 23)
        pickerViewLable.text = uiElements[row]
        
        return pickerViewLable
        
    }
    
    
}

