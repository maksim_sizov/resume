//
//  OrderStatus.swift
//  service_helper
//
//  Created by MaxOS on 13.04.2023.
//

import Foundation




enum OrderStatus: String, CaseIterable  {
    case new = "Заявка"
    case cooking = "Сборка"
    case delivery = "Доставка"
    case complite = "Готово к получению"
    case cancelled = "Выдано"
}
