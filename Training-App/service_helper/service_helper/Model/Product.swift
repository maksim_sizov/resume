//
//  Product.swift
//  service_helper
//
//  Created by MaxOS on 04.04.2023.
//

import Foundation
import FirebaseFirestore


struct Product {

    
    
    
    var id:             String
    var title:          String
    var ImageUrl:       String = ""
    var price:          Int
    var description:    String
    var documentNumber: String
  
    
    
    var representaton: [String: Any] {
        
        var repres = [String: Any]()
        
        repres["id"] = self.id
        repres["title"] = self.title
        repres["price"] = self.price
        repres["description"] = self.description
        repres["documentNumber"] = self.documentNumber
        
        return repres
    }
    internal init(id: String = UUID().uuidString,
                  title: String,
                  ImageUrl: String = "",
                  price: Int,
                  description: String,
                  documentNumber: String) {
        self.id = id
        self.title = title
        self.ImageUrl = ImageUrl
        self.price = price
        self.description = description
        self.documentNumber = documentNumber
    }
    init?(doc: QueryDocumentSnapshot) {
        
        let data = doc.data()
        guard let id = data["id"] as? String else { return nil }
        guard let title = data["title"] as? String else { return nil }
        guard let price = data["price"] as? Int else { return nil }
        guard let description = data["description"] as? String else { return nil }
        guard let documentNumber = data["documentNumber"] as? String else { return nil }
        
        self.id = id
        self.title = title
        self.price = price
        self.description = description
        self.documentNumber = documentNumber
    }
    
}


