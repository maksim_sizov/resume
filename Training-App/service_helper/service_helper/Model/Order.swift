//
//  Order.swift
//  service_helper
//
//  Created by MaxOS on 13.04.2023.
//

import Foundation
import FirebaseFirestore
import SwiftUI


struct Order {
    
    var id: String = UUID().uuidString
    var userId: String
    var positions = [Position]()
    var date: Date
    var status: String
    var cost: String {
        var sum = 0
        var sumString: String = ""
        for position in positions {
            sum += position.cost
            sumString = String(sum)
        }
        return sumString
    }
    func convertDataDay (date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let stringDate = formatter.string(from: date)
        return stringDate
    }
    func convertDataTime (date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let stringDate = formatter.string(from: date)
        return stringDate
    }
    
    var representation : [String: Any] {
        var repres = [String:Any]()
        repres["id"] = id
        repres["userId"] = userId
        repres["date"] = Timestamp(date: date)
        repres["status"] = status
        return repres
    }
    init(id: String = UUID().uuidString,
         userId: String,
         positions: [Position] = [Position](),
         date: Date,
         status: String) {
        
        self.id = id
        self.userId = userId
        self.positions = positions
        self.date = date
        self.status = status
        
    }
    
    init?(doc: QueryDocumentSnapshot) {
        let data = doc.data()
        
        guard let id = data["id"] as? String else { return nil}
        guard let userId = data["userId"] as? String else { return nil}
        guard let date = data["date"] as? Timestamp else { return nil}
        guard let status = data["status"] as? String else { return nil}
        
        self.id = id
        self.userId = userId
        self.date = date.dateValue()
        self.status = status
    }
    
}
