//
//  MSUser.swift
//  service_helper
//
//  Created by MaxOS on 12.04.2023.
//

import Foundation
import FirebaseFirestore



struct MSUser: Identifiable {
    
    var id: String
    var name: String
    var phone: Int
    var address: String
    
    var representaton: [String: Any] {
        
        var repres = [String: Any]()
        
        repres["id"] = self.id
        repres["name"] = self.name
        repres["phone"] = self.phone
        repres["address"] = self.address
        
        return repres
    }
    
}
