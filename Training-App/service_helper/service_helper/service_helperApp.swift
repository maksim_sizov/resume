//
//  service_helperApp.swift
//  service_helper
//
//  Created by MaxOS on 02.04.2023.
//

import SwiftUI
import Firebase
import FirebaseAuth


let screen = UIScreen.main.bounds


@main
struct service_helperApp: App {
    
    @UIApplicationDelegateAdaptor private var appDelegate: AppDelegate
    
    var body: some Scene {
        WindowGroup {
            if let user = AuthService.shared.currentUser {
                if user.uid == "N9YD8VQ3CCRrV76CkmAXi7C9kXv2" {
                    AdminOrdersView()
                } else {
                    let viewModel = MainTabBarViewModel(user: user)
                    MainTabBar(viewModel: viewModel)
                }
            } else {
                AuthView()
            }
            
          
        }
    }
    class AppDelegate: NSObject, UIApplicationDelegate {
        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
                
            FirebaseApp.configure()
            print("OK") 
            
            return true
        }
    }

}
