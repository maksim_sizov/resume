//
//  ProductDetailView.swift
//  service_helper
//
//  Created by MaxOS on 04.04.2023.
//

import SwiftUI

struct ProductDetailView: View {
    
    @State var viewModel: ProductDetailViewModel 
//    @State var size = "Маленькая"
    @State var count = 1
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        
        VStack {
            VStack(alignment: .leading){
                Image(uiImage: self.viewModel.image)
                    .resizable()
                    .frame(maxWidth: .infinity, maxHeight: 260)
                HStack{
                    VStack(alignment: .leading){
                        Text("\(viewModel.product.title)")
                            .font(.title2.bold())
                        Text("Номер детали: \(viewModel.product.documentNumber)")
                    }
                    Spacer()
                    
                    Text("\(viewModel.product.price) ₽") //(size: self.size)
                        .font(.title2)
                    
                }.padding(.horizontal)
                Text("\(viewModel.product.description)")
                    .padding(.horizontal)
                    .padding(.vertical, 4)
                
                HStack{
                    Stepper("Колличество", value: $count, in: 1...10)
                    Text("\(self.count)")
                        .padding(.leading, 32)
                    
                    
                }.padding(.horizontal)
                
//                Picker("Размер пиццы", selection: $size) {
//                    ForEach(viewModel.sizes, id: \.self){ item in
//                        Text(item)
//                    }
//                    
//                }.pickerStyle(.segmented)
//                    .padding(5)
            }
            Button {
                var position = Position(id: UUID().uuidString, product: viewModel.product, count: self.count)
                position.product.price = viewModel.product.price //.getPrice(size: size)
                CardViewModel.shared.addPosition(position)
                presentationMode.wrappedValue.dismiss()
                
            } label: {
                Text("В корзину")
                    .padding()
                    .padding(.horizontal, 60)
                    .font(.title3.bold())
                    .cornerRadius(30)
                    .foregroundColor(.white)
                    .background(.blue)
            }
            .onAppear {
                self.viewModel.getImage()
            }

            Spacer()
        }
    }
}

struct ProductDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ProductDetailView(viewModel: ProductDetailViewModel(product: Product(title: "Cвечи зажигания", price: 1380, description: "Сечи зажигания для мотоцикла,  в упаковке 4 штуки производитель Brembo", documentNumber: "301-983-354")
        ))
    }
}
