//
//  AdminProfile.swift
//  service_helper
//
//  Created by MaxOS on 22.04.2023.
//

import SwiftUI

struct AdminProfile: View {
    var body: some View {
        // Информация об администраторе
        VStack{
            HStack{
                Text("Ваше имя: ")
                    .bold()
            }
            HStack{
                Text("Номер телефона: ")
                    .bold()
            }
            HStack{
                Text("Адрес магазина: ")
                    .bold()
            }
            HStack{
                Text("Номер магазина: ")
                    .bold()
            }
            HStack{
                Text("Ваша должность: ")
                    .bold()
            }
        }
    }
}

struct AdminProfile_Previews: PreviewProvider {
    static var previews: some View {
        AdminProfile()
    }
}
