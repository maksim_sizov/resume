//
//  ContentView.swift
//  service_helper
//
//  Created by MaxOS on 02.04.2023.
//

import SwiftUI

struct AuthView: View {
    
    @State private var isAuth = true
    @State private var email = ""
    @State private var password = ""
    @State private var confirmPassword = ""
    @State private var isTabViewShow = false
    @State private var isShowAlert = false
    @State private var alertMassage = ""
    
    
    var body: some View {
        VStack (spacing: 40) {
            Text(isAuth ? "Авторизация" : "Регистрация")
                .padding()
                .padding(.horizontal, 80)
                .font(.title.bold())
                .foregroundColor(Color(.black))
                .background(Color("whiteAlfaButton"))
                .cornerRadius(30)
            
            VStack {
                // Поле ввода номера телефона
                TextField("Введите email", text: $email)
                    .padding()
                    .background(Color("whiteAlfa"))
                    .cornerRadius(30)
                    .padding(8)
                    .padding(.horizontal, 12)
                
                // Поле ввода пароля
                SecureField("Введите пароль", text: $password)
                    .padding()
                    .background(Color("whiteAlfa"))
                    .cornerRadius(30)
                    .padding(8)
                    .padding(.horizontal, 12)
                if !isAuth {
                    
                    SecureField("Повторите пароль", text: $confirmPassword)
                        .padding()
                        .background(Color("whiteAlfa"))
                        .cornerRadius(30)
                        .padding(8)
                        .padding(.horizontal, 12)
                }
                
                // Кнопка авторизации
                Button {
                    if isAuth {
                        print("Авторизация пользователя")
                        
                        AuthService.shared.signIn(email: self.email,
                                                  password: self.password) { result in
                            switch result {
                            case .success(_):
                                isTabViewShow.toggle()
                            case .failure(let error):
                                alertMassage = "Ошибка авторизации \(error.localizedDescription)"
                                isShowAlert.toggle()
                            }
                        }
                        
                        
                        
                        
                    } else {
                        print("Регистрация пользователя")
                        
                        guard password == confirmPassword else {
                            self.alertMassage = "Неверный пароль"
                            self.isShowAlert.toggle()
                            return
                        }
                        
                        AuthService.shared.singUp(email: self.email,
                                                   password: self.password) { result in
                            switch result {
                            case .success(let user):
                                alertMassage = "Вы зарегистрировались с email \(user.email!)"
                                self.isShowAlert.toggle()
                                self.email = ""
                                self.password = ""
                                self.confirmPassword = ""
                                self.isAuth.toggle()
                                
                            case .failure(let error):
                                alertMassage = "Ошибка регистрации \(error.localizedDescription)"
                                self.isShowAlert.toggle()
                            }
                            
                        }
                        
                    }
                    
                } label: {
                    Text(isAuth ? "Войти" : "Создать аккаунт")
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(LinearGradient(colors: [Color("blue"), Color(.white)], startPoint: .leading, endPoint: .trailing))
                        .cornerRadius(30)
                        .padding(8)
                        .padding(.horizontal, 12)
                        .font(.title2.bold())
                        .foregroundColor(.black)
                }
                
                //Кнопка регистрации
                Button {
                    isAuth.toggle()
                } label: {
                    Text(isAuth ? "Регистрация" : "Уже есть аккаунт?")
                        .padding()
                        .frame(maxWidth: .infinity)
                        .cornerRadius(30)
                        .padding(8)
                        .padding(.horizontal, 12)
                        .font(.title.bold())
                        .foregroundColor(Color(.black))
                }
                
            }
            .padding()
            .background(Color("whiteAlfaButton"))
            .cornerRadius(30)
            .padding()
            .alert(alertMassage, isPresented: $isShowAlert) {
                
                Button { } label: {
                    Text("Ok")
                }
                
            }
            
            
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
        
            .background(Image("background")
                .ignoresSafeArea()
                .blur(radius: isAuth ? 5 : 50)
            )
            .animation(Animation.easeInOut(duration: 0.3), value: isAuth)
            .fullScreenCover(isPresented: $isTabViewShow) {
                
                if AuthService.shared.currentUser?.uid == "N9YD8VQ3CCRrV76CkmAXi7C9kXv2" {
                    AdminOrdersView()
                    
                } else {
                    let mainTabBarViewModel = MainTabBarViewModel(user:
                    AuthService.shared.currentUser!)
                    
                    
                    MainTabBar(viewModel: mainTabBarViewModel)
                }

                
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        AuthView()
    }
}
