//
//  mainTabBar.swift
//  service_helper
//
//  Created by MaxOS on 03.04.2023.
//

import SwiftUI

struct MainTabBar: View {
    var viewModel: MainTabBarViewModel
    var body: some View {
            
        TabView {

            NavigationView {
                CatalogView()
            }
                    .tabItem {
                        VStack {
                            Image(systemName: "cart")   
                            Text("Товары магазина")
                        }
                        
                    }
            CardView(viewModel: CardViewModel.shared)
                    .tabItem {
                        VStack {
                            Image(systemName: "basket")
                            Text("Корзина товаров")
                        }
                        
                    }
                
            MyProfileView(viewModel: ProfileViewModel(profile: MSUser(id: " ",
                                                                      name: "Vasy",
                                                                      phone: 8999888776,
                                                                      address: "Moscow")))
                    .tabItem {
                        VStack {
                            Image(systemName: "person.text.rectangle")
                            Text("Профиль")
                        }
                    }
            }
        }
    }
