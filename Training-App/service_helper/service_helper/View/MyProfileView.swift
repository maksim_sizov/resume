//
//  MyProfileView.swift
//  service_helper
//
//  Created by MaxOS on 03.04.2023.
//

import SwiftUI

struct MyProfileView: View {
    
    @State var isAvaAlertPresented = false
    @State var isQuitAlertPresented = false
    @State var isAuthViewPresented = false
    @State var isOrderViewShow = false
    @StateObject var viewModel: ProfileViewModel
    
    
    var body: some View {
        
        VStack(alignment: .center) {
            HStack(spacing: 20){
                Image("user")
                    .resizable()
                    .frame(width: 80, height: 80)
                    .onTapGesture {
                        isAvaAlertPresented.toggle()
                    }.confirmationDialog("Откуда взять фото", isPresented: $isAvaAlertPresented) {
                        Button {
                            print("Labrary")
                        } label: {
                            Text("Из галереи")
                        }
                        Button {
                            print("Camera")
                        } label: {
                            Text("Снять фото")
                        }
                        
                    }
                VStack(alignment: .leading, spacing: 12) {
                    TextField("Имя", text: $viewModel.profile.name)
                        .bold()
                    HStack{
                        Text("Телефон: +7 ")
                        TextField("Телефон", value: $viewModel.profile.phone, format: IntegerFormatStyle.number)
                    }
                }
            }
            VStack(alignment: .leading, spacing: 10) {
                Text("Пункт выдачи товара:")
                    .bold()
                TextField("Ваш адресс", text: $viewModel.profile.address)
            }.padding(.horizontal)
            List {
                if viewModel.orders.count == 0 {
                    Text("Ваши заказы")
                } else {
                    ForEach(viewModel.orders, id: \.id) { order in
                        OrderCell(order: order)
                    }.onTapGesture {
                        isOrderViewShow.toggle()
                    }
                }

            }
            .listStyle(.plain)
                .onAppear{
                    viewModel.getOrders()
                }
                .sheet(isPresented: $isOrderViewShow) {
                    Text("Тут будет информация о вашем заказе\nКогда:\nЧего:\nНомер заказа:")

                }
            Button {
                isQuitAlertPresented.toggle()
            } label: {
                Text("Выйти")
                    .padding(.horizontal, 60)
                    .padding(.vertical)
                    .background(Color.blue)
                    .cornerRadius(20)
                    .foregroundColor(.white)
                
            }.padding()
                .actionSheet(isPresented: $isQuitAlertPresented, content: {
                    ActionSheet(title: Text("Вы уверены?"), message: Text("Нажмите нет, если не хотите выходить."), buttons: [.destructive(Text("Да"), action: {
                        isAuthViewPresented.toggle()
                    }), .cancel(Text("Нет"))])
                })
                .fullScreenCover(isPresented: $isAuthViewPresented, onDismiss: nil) {
                    AuthView()
                }
            
        }
        .onSubmit {
            viewModel.setProfile()
        }
        .onAppear {
            self.viewModel.getProfile()
            self.viewModel.getOrders()
        }
    }
}

struct MyProfileView_Previews: PreviewProvider {
    static var previews: some View {
        MyProfileView(viewModel: ProfileViewModel(profile: MSUser(id: "",
                                                                    name: "Vasya",
                                                                    phone: 8999777663,
                                                                    address: "Moscow")))
    }
}
