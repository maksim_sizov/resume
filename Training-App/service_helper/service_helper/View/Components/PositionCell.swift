//
//  PositionSell.swift
//  service_helper
//
//  Created by MaxOS on 05.04.2023.
//

import SwiftUI

struct PositionCell: View {
    
    let position: Position
    var body: some View {
        
            HStack {
                VStack(alignment: .leading) {
                Text(position.product.title)
                    .fontWeight(.bold)
                    .font(.title3)
                    Text("ID: \(position.product.documentNumber)")
            }
                Spacer()
                Text("\(position.count) шт.")
                    .font(.title3)
                Text("\(String(position.cost))₽")
                    .font(.title2)
                    .frame(width: 85, alignment: .trailing)
                
        }.padding(.horizontal)
    }
}

struct PositionCell_Previews: PreviewProvider {
    static var previews: some View {
        PositionCell(position: Position(id: UUID().uuidString, product:
                                            Product(id: UUID().uuidString,
                                                    title: "Шиномонтаж",
                                                    ImageUrl: "whileReplace",
                                                    price: 13500,
                                                    description: "Шиномонтаж",
                                                    documentNumber: "331-330-3331"), count: 1))
    }
}
