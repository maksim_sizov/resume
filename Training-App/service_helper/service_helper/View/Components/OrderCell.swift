//
//  OrderSell.swift
//  service_helper
//
//  Created by MaxOS on 14.04.2023.
//

import SwiftUI
import UIKit


struct OrderCell: View {
    var order: Order
    
    var body: some View {
        
            HStack {
                VStack(alignment: .leading) {
                        Text("Время: \(order.convertDataTime(date: order.date))")
                            .bold()
                        Text("Дата: \(order.convertDataDay(date: order.date))")
                    
                }
                .padding()
                .frame(width: screen.width / 2)
                
                VStack(alignment: .center) {
                    Text("К оплате: \(order.cost) ₽")
                        .bold()
                    
                    Text("\(order.status)")
                    
                        .foregroundColor(.green)
                }.frame(width: screen.width / 2)
            }
    }
}

//struct OrderCell_Previews: PreviewProvider {
//    static var previews: some View {
//        OrderCell()
//    }
//}
