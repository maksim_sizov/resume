//
//  ProductSell.swift
//  service_helper
//
//  Created by MaxOS on 04.04.2023.
//

import SwiftUI

struct ProductCell: View {
    
    var product: Product
    @State var uiImage = UIImage(named: "whileReplace")
    
    var body: some View {
        VStack (spacing: 2) {
            Image(uiImage: uiImage!)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(maxWidth: screen.width * 0.45, maxHeight:  screen.width * 0.5)
                .clipped()
                .cornerRadius(12)
            HStack {
                Text (product.title)
                    .font(.custom("AvenirNext-regular", size: 10))
                Spacer()
                Text("\(product.price)₽")
                    .font(.custom("Avenir-bold", size: 15))
            }.padding(.horizontal, 3)
                .padding(.bottom, 6)
        }.frame(width: screen.width * 0.45 , height: screen.width * 0.55)
            .background(Color(.white))
            .cornerRadius(12)
            .shadow(radius: 5)
            .onAppear{
                StorageService.shared.downloadProductImage(id: self.product.id) { result in
                    switch result {
                        
                    case .success( let data ):
                        if let img = UIImage(data: data) {
                            self.uiImage = img
                        }
                    case .failure( let error ):
                        print(error.localizedDescription)
                    }
                }
            }

    }
}

struct ProductCell_Previews: PreviewProvider {
    static var previews: some View {
        ProductCell(product: Product(id: "1",
                                     title: "Шины",
                                     ImageUrl: "Not found",
                                     price: 4000,
                                     description: "Сountinental летние",
                                    documentNumber: " "))
    }
}
