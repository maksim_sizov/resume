//
//  MyRepair.swift
//  service_helper
//
//  Created by MaxOS on 03.04.2023.
//

import SwiftUI
import FirebaseFirestore

struct CardView: View {
    
    @StateObject var viewModel : CardViewModel
    @State private var isShowAlert = false
    @State private var alertMassage = ""
    @State private var isShowAgree = false
    
    var body: some View {
        VStack{
            List(viewModel.position) { position in
                PositionCell(position: position)
                    .swipeActions {
                        Button {
                            viewModel.position.removeAll { pos in
                                pos.id == position.id
                            }
                        } label: {
                            Text("Удалить")
                        }.tint(.red)

                    }
            }
            .listStyle(.plain)
            .navigationTitle("Корзина")
            
            HStack {
                Text("Итого:")
                    .fontWeight(.bold)
                Spacer()
                Text("\(self.viewModel.cost) ₽")
                    .fontWeight(.bold)
            }.padding()
            
            HStack(spacing: 24) {

                Button {
                    if viewModel.position.isEmpty != true {
                        self.isShowAgree.toggle()
                    } else {
                        self.alertMassage = "Добавьте товар в корзину"
                        self.isShowAlert.toggle()
                    }
                } label: {
                    Text("Заказать")
                }
                .alert(isPresented: $isShowAgree, content: {
                    Alert(title: Text("Заказать выбраный товар?"), primaryButton: .destructive(Text("Нет")), secondaryButton: .default(Text("Да"), action: {
                        var order = Order(userId: AuthService.shared.currentUser!.uid,
                                          date: Date(),
                                          status: OrderStatus.new.rawValue)
                        order.positions = self.viewModel.position
                        DatabaseService.shared.setOrder(order: order) { result  in
                            switch result {
                                
                            case .success(let order):
                                print(order.cost)
                            case .failure(let error):
                                print(error.localizedDescription)
                            }
                        }
                        while viewModel.position.isEmpty != true {
                            viewModel.position.remove(at: 0)
                        }
                        print("Заказать")
                    }))
                })
                    .font(.body)
                    .fontWeight(.bold)
                    .padding()
                    .frame(maxWidth: .infinity)
                    .foregroundColor(.white)
                    .background(Color(.blue))
                    .cornerRadius(24)
                    .padding()
                    .alert(alertMassage, isPresented: $isShowAlert) {

                        Button { } label: {
                            Text("Ok")
                        }
                        
                    }
            }
            }
        }
    
    struct CardView_Previews: PreviewProvider {
        static var previews: some View {
            CardView(viewModel: CardViewModel.shared)
        }
    }
}
