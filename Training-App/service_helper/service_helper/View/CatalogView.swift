//
//  Car.swift
//  service_helper
//
//  Created by MaxOS on 03.04.2023.
//

import SwiftUI

struct CatalogView: View {
    
    let layoutForPopularProduct = [GridItem(.adaptive(minimum: screen.width / 2.2))]
    let layoutForProduct = [GridItem(.adaptive(minimum: screen.width / 2.4))]
    @StateObject var viewModel = CatalogViewModel()
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            Section ("Акции и скидки") {
                ScrollView (.horizontal, showsIndicators: false){
                    LazyHGrid(rows: layoutForPopularProduct, spacing: 12) {
                        ForEach(viewModel.products, id: \.id) { item in
                            NavigationLink {
                                let viewModel = ProductDetailViewModel(product: item)
                                ProductDetailView(viewModel: viewModel)
                            } label: {
                                ProductCell(product: item)
                                    .foregroundColor(.black)
                            }

                            }
                        }.padding()
                    }
                }
            
            Section ("Товары") {
                ScrollView (.vertical, showsIndicators: false){
                    LazyVGrid(columns: layoutForProduct, spacing: 12) {
                        ForEach(viewModel.products, id: \.id) { item in
                            NavigationLink {
                                let viewModel = ProductDetailViewModel(product: item)
                                ProductDetailView(viewModel: viewModel)
                            } label: {
                                ProductCell(product: item)
                                    .foregroundColor(.black)
                            }
                        }
                    }.padding()
                    
                }
            }
        } .onAppear {
        
            viewModel.getProducts()
        }
        }
    }

struct Car_Previews: PreviewProvider {
    static var previews: some View {
        CatalogView()
    }
}
