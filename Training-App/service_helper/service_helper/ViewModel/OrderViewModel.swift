//
//  OrderViewModel.swift
//  service_helper
//
//  Created by MaxOS on 17.04.2023.
//

import Foundation


class OrderViewModel: ObservableObject {
    
    @Published var order: Order
    @Published var user = MSUser(id: "", name: "", phone: 0, address: "")
    
    init(order: Order) {
        self.order = order
    }
    func getUserData() {
        DatabaseService.shared.getProfile(by: order.userId) { result in
            switch result {
            case .success(let user):
                self.user = user
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
}
