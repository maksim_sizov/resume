//
//  CatalogViewModel.swift
//  service_helper
//
//  Created by MaxOS on 04.04.2023.
//

import Foundation


class CatalogViewModel: ObservableObject {
    
    static let shared = CatalogViewModel()
    
    var popularProducts = [                     Product(id: "1",
                                                        title: "Колодки на ВАЗ 2106",
                                                        ImageUrl: "Not found",
                                                        price: 1350,
                                                        description: "комплект 4 шт",
                                                        documentNumber: "8943" ),
                                                    
                         
                                                Product(id: "2",
                                                        title: "Антифриз зеленый 5 литров",
                                                        ImageUrl: "Not found",
                                                        price: 2000,
                                                        description: "немецкий авнтифриз",
                                                        documentNumber: "943"),
                                                
                                                Product(id: "3",
                                                        title: "Трубка полиуретановая",
                                                        ImageUrl: "Not found",
                                                        price: 500,
                                                        description: "Для системы охлаждения",
                                                        documentNumber: "8943" ),
                                                
                                                Product(id: "4",
                                                        title: "Масло Castrol",
                                                        ImageUrl: "Not found",
                                                        price: 1500,
                                                        description: "MAGNATEC",
                                                        documentNumber: "8943" )
        ]
    @Published var products = [                  Product(id: "1",
                                                         title: "Колодки на ВАЗ 2106",
                                                         ImageUrl: "Not found",
                                                         price: 1350,
                                                         description: "комплект 4 шт",
                                                         documentNumber: "8943" ),
                           
                                                  Product(id: "2",
                                                          title: "Антифриз зеленый 5 литров",
                                                          ImageUrl: "Not found",
                                                          price: 2000,
                                                          description: "немецкий авнтифриз",
                                                          documentNumber: "8943" ),
                                                 
                                                  Product(id: "3",
                                                          title: "Трубка полиуретановая",
                                                          ImageUrl: "Not found",
                                                          price: 500,
                                                          description: "Для системы охлаждения",
                                                          documentNumber: "8943" ),
                                                 
                                                  Product(id: "4",
                                                          title: "Масло Castrol",
                                                          ImageUrl: "Not found",
                                                          price: 1500,
                                                          description: "MAGNATEC",
                                                          documentNumber: "8943" )
        ]
    func getProducts() {
        DatabaseService.shared.getProducts { result in
            switch result {
                
            case .success(let product):
                self.products = product
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
