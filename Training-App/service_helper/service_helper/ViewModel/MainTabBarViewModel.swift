//
//  mainTabBarViewModel.swift
//  service_helper
//
//  Created by MaxOS on 10.04.2023.
//

import Foundation
import FirebaseAuth

class MainTabBarViewModel: ObservableObject {
    @Published var user: User
    
    init(user: User) {
        
        self.user = user
    }
}
