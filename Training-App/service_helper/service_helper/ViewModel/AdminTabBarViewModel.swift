//
//  AdminTabBarViewModel.swift
//  service_helper
//
//  Created by MaxOS on 23.04.2023.
//

import Foundation
import FirebaseAuth

class AdminTabBarViewModel: ObservableObject {
    @Published var user: User
    
    init(user: User) {
        
        self.user = user
    }
}
