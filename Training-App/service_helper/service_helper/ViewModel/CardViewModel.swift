//
//  CardViewModel.swift
//  service_helper
//
//  Created by MaxOS on 06.04.2023.
//

import Foundation



class CardViewModel: ObservableObject {
    @Published var position = [Position]()
    static let shared = CardViewModel()
    private init() { }
    var cost: Int {
        var sum = 0
        for pos in position {
            sum += pos.cost
        }
        return sum
    }
    
    func addPosition(_ position: Position) {
        self.position.append(position)
    }
}
