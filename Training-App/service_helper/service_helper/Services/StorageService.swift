//
//  StorageService.swift
//  service_helper
//
//  Created by MaxOS on 19.04.2023.
//

import Foundation
import FirebaseStorage


class StorageService {
    static var shared = StorageService()
    
    private init() { }
    
    private let storage = Storage.storage().reference()
    private var productRef: StorageReference { storage.child("products") }
    
    func upload(id: String, image: Data, complition : @escaping (Result<String, Error>) -> ()) {
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpg"
        
        productRef.child(id).putData(image,metadata: metadata) { metadata, error in
            guard let metadata = metadata else {
                if let error = error {
                    complition(.failure(error))
                }
                
                return
            }
            
            complition(.success("Размер полученого изображения \(metadata.size)"))
        }
    }
    func downloadProductImage(id: String, complition: @escaping (Result<Data, Error>) -> ()) {
        productRef.child(id).getData(maxSize: 2 * 1024 * 1024) { data, error in
            guard let data = data else {
                if let error = error {
                    complition(.failure(error))
                }
                return
            }
            complition(.success(data))
        }
    }
}
