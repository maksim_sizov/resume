//
//  AuthService.swift
//  service_helper
//
//  Created by MaxOS on 10.04.2023.
//

import Foundation
import FirebaseAuth

class AuthService {
    
    static let shared = AuthService()
    
    private init() { }
    
    private let auth = Auth.auth()
    
    var currentUser: User? {
        return auth.currentUser
    }
    
    func singOut() {
        try! auth.signOut()
    }
    func singUp(email: String,
                 password: String,
                 complition: @escaping (Result<User, Error>) -> ()) {
        
        auth.createUser(withEmail: email,
                        password: password) { result, error in
            if let result = result {
                
                let msUser = MSUser(id: result.user.uid,
                                    name: " ",
                                    phone: 0,
                                    address: " ")
                
                DatabaseService.shared.setProfile(user: msUser) { resultDB  in
                    switch resultDB {
                        
                    case .success(_):
                        complition(.success(result.user))
                    case .failure(let error):
                        complition(.failure(error))
                    }
                }
                
            } else if let error = error {
                complition(.failure(error))
            }
        }
    }
    
    
    func signIn(email: String, password: String,
                 complition: @escaping (Result<User, Error>) -> ()) {
        auth.signIn(withEmail: email, password: password) { result, error in
            if let result = result {
                complition(.success(result.user))
            } else if let error = error {
                complition(.failure(error))
            }
        }
    }
}
