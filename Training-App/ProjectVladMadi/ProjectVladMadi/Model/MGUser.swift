//
//  MGUser.swift
//  ProjectVladMadi
//
//  Created by MaxOS on 12.04.2023.
//

import Foundation





struct MGUser: Identifiable {
    
    var id: String
    var name: String
    var phone: Int
    var address: String
    
}
