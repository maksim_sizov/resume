//
//  ProjectVladMadiApp.swift
//  ProjectVladMadi
//
//  Created by MaxOS on 26.03.2023.
//

import SwiftUI

let screen = UIScreen.main.bounds

@main
struct ProjectVladMadiApp: App {
    var body: some Scene {
        WindowGroup {
            StartView()
        }
    }
}
