//
//  GameNew.swift
//  ProjectVladMadi
//
//  Created by MaxOS on 26.03.2023.
//

import SwiftUI

struct GameView: View {
    
    @State private var word = ""
    
    var viewModel: GameViewModel
    @Environment(\.dismiss) var dismiss
    
    @State private var confirmPresent = false
    @State private var isAlertPresent = false
    @State var alertText = ""
    
    var body: some View {
        VStack(spacing: 16) {
            HStack {
                Button {
                    confirmPresent.toggle()
                } label: {
                    Text("Выход")
                        .padding(6)
                        .padding(.horizontal)
                        .background(Color("Orange"))
                        .cornerRadius(12)
                        .padding(6)
                        .foregroundColor(.white)
                        .font(.custom("AvenirNext-Bold",
                                      size: 18))
                }
                Spacer()
                
            }
            Text(viewModel.word)
                .font(.custom("AvenirNext-Bold",
                              size: 30))
                .foregroundColor(.white)
            HStack (spacing: 12 ) {
                VStack{
                    Text("\(viewModel.player1.score)")
                        .font(.custom("AvenirNext-Bold", size: 60))
                        .foregroundColor(.white)
                    Text("\(viewModel.player1.name)")
                        .font(.custom("AvenirNext-Bold", size: 24))
                        .foregroundColor(.white)
                }.padding(20)
                    .frame(width: screen.width / 2.2,
                           height: screen.width / 2.2)
                    .background(Color("FirstPlayer"))
                    .cornerRadius(26)
                    .shadow(color: viewModel.isFirst ? .red : .white,
                            radius: 4,
                            x: 0,
                            y: 0)
                
                VStack{
                    Text("\(viewModel.player2.score)")
                        .font(.custom("AvenirNext-Bold", size: 60))
                        .foregroundColor(.white)
                    Text("\(viewModel.player2.name)")
                        .font(.custom("AvenirNext-Bold", size: 24))
                        .foregroundColor(.white)
                }.padding(20)
                    .frame(width: screen.width / 2.2,
                           height: screen.width / 2.2)
                    .background(Color("SecondColor"))
                    .cornerRadius(26)
                    .shadow(color: viewModel.isFirst ? .white : .purple,
                            radius: 4,
                            x: 0,
                            y: 0)
            }
            WordTextField(word: $word, placeholder: "Ваше слово")
                .padding(.horizontal)
            
            Button {
                var score = 0
                do{
                    try score = viewModel.check(word: word)
                } catch WordError.beforeWord {
                        alertText = "Включи логику, придумай новое слово!"
                        isAlertPresent.toggle()
                } catch WordError.litleWord {
                    alertText = "Cлишком короткое слово!"
                    isAlertPresent.toggle()
                } catch WordError.theSameWord {
                    alertText = "Думавешь самый умный? Составленное слово не должно быть исходным словом!"
                    isAlertPresent.toggle()
                } catch WordError.wrongWord {
                    alertText = "Такое слово не может быть составлено!"
                    isAlertPresent.toggle()
                } catch {
                    alertText = "Неизвестная ошибка"
                    isAlertPresent.toggle()
                }
                if score > 1 {
                    self.word = ""
                }
            } label: {
                Text("Готово")
                    .padding(12)
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity)
                    .background(Color("Orange"))
                    .cornerRadius(16)
                    .font(.custom("AvenirNext-Bold", size: 26))
                    .padding(.horizontal)
                    .shadow(radius: 3)
                
            
        }
            List {
            ForEach(0 ..< self.viewModel.words.count, id: \.description) { item in
                WordCell(word: self.viewModel.words[item])
                    .background(item % 2 == 0 ? Color("FirstPlayer") : Color("SecondColor"))
                    .listRowInsets(EdgeInsets())
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .shadow(radius: 10, y: 3)

            }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .cornerRadius(16)
            .colorMultiply(self.viewModel.words.count < 1 ? .clear : .white )
            .shadow(radius: 3, y: -3)
                
    }
            
.background(Image("background"))
            .confirmationDialog("Вы уверены? Хотите завершить игру?",
                                isPresented: $confirmPresent, titleVisibility: .hidden){
                Button(role: .destructive) {
                    self.dismiss()
                } label: {
                    Text("Да")
                }
                Button(role: .cancel) { } label: {
                    Text("Нет")
                }
                
            }
            .alert(alertText,
            isPresented: $isAlertPresent) {
            Text("Ок, понял...")
            }
    }
}

struct GameNew_Previews: PreviewProvider {
    static var previews: some View {
        GameView(viewModel: GameViewModel(player1: Player(name: "Вася"), player2: Player(name: "Петя") , word: "Магнитотерапия" ))
    }
}
