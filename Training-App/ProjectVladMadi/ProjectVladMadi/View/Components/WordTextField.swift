//
//  WordTextField.swift
//  ProjectVladMadi
//
//  Created by MaxOS on 26.03.2023.
//

import SwiftUI

struct WordTextField: View {
    @State var word: Binding<String>
    var placeholder: String
    
    var body: some View {
        TextField(placeholder, text: word)
            .font(.title2)
            .padding()
            .background(.white)
            .cornerRadius(20)
    }
}
