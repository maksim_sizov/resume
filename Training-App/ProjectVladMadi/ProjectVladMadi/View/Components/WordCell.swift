//
//  WordCell.swift
//  ProjectVladMadi
//
//  Created by MaxOS on 28.03.2023.
//

import SwiftUI

struct WordCell: View {
    
    let word: String
    var body: some View {
        HStack {
            Text(word)
                .foregroundColor(.white)
                .listRowSeparator(.hidden)
                .frame(maxWidth: .infinity,
                       maxHeight: . infinity,
                       alignment: .leading)
                .padding()
                .font(.custom("AvenirNext-bold", size: 22))
            Text("\(word.count)")
                .font(.custom("AvenirNext-bold", size: 22))
                .foregroundColor(.white)
                .padding()
        }
    }
}

struct WordCell_Previews: PreviewProvider {
    static var previews: some View {
        WordCell(word: " ")
    }
}
